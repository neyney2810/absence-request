sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageToast"
], function(Controller, JSONModel, MessageToast) {
	"use strict";

	return Controller.extend("Z03_AbsenceRequest.controller.MainView", {
		onInit: function() {
			// set explored app's demo model on this sample
			this.getView().setModel(new JSONModel(), "app");
		},
		onOpenDialog: function() {
			/*	// create dialog lazily
				if (!this.pDialog) {
					this.pDialog = this.loadFragment({
						name: "Z03_AbsenceRequest.view.UrlaubRequestInput"
				});
				
				} 
				this.pDialog.then(function(oDialog) {
					oDialog.open();
				});*/

			var oView = this.getView();
			var oDialog = oView.byId("urlaubRequestInput");
			// create dialog lazily
			if (!oDialog) {
				// create dialog via fragment factory
				oDialog = sap.ui.xmlfragment(oView.getId(), "Z03_AbsenceRequest.view.UrlaubRequestInput", this);
				// connect dialog to view (models, lifecycle)
				oView.addDependent(oDialog);
			}

			oDialog.open();
		},
		onCloseDialog: function() {
			this.getView().byId("urlaubRequestInput").close();
		},
		onSave: function(event) {
			var oModel = this.getOwnerComponent().getModel();
			
			var startDate = new Date(this.getView().byId("input-begin-date").getValue());
			var endDate = new Date(this.getView().byId("input-end-date").getValue());
			var oData = {
				"StartDate": startDate,
				"EndDate": endDate,
				"Type": this.getView().byId("input-art").getSelectedKey(),
				"Description": this.getView().byId("description").getValue()
			};
			oModel.create("/AbsenceEntriesSet", oData, {
				success: jQuery.proxy(function() {
					var message = this.getView().getModel("i18n").getResourceBundle().getText("message.create.success");
					MessageToast.show(message);
					this.onCloseDialog();
				}, this),
				error: jQuery.proxy(function() {
						var message = this.getView().getModel("i18n").getResourceBundle().getText("message.create.error");
						MessageToast.show(message);
					},
					this)
			});
		}

	});
});